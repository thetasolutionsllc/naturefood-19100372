#Uncomment these to run the first time so the packages are installed.
#using Pkg
#Pkg.add("DataFrames")
#Pkg.add("JuMP")
#Pkg.add("GLPK")
#Pkg.add("CSV")
#Pkg.add("LinearAlgebra")

using DataFrames;
using JuMP
using GLPK
using CSV;
using LinearAlgebra;


#When running inside Juno need to change the working directory to the directory where the source file lives
#May not work on windows, see https://discourse.julialang.org/t/how-to-change-working-directory-in-julia-with-atom/7588/7
# println(pwd())
cd(dirname(@__FILE__))
# println(pwd())


##################
#User inputs
##################
#Change the analysisPreset to 1,2,3,4, or 5 to run the analyses from the paper
#Preset analysis output directories are "analysis1", "analysis2", "analysis3", "analysis4", "analysis5"
analysisPreset = 1
#For analysisPreset = 2 adjust the cost scale factor below to 1.05 (5% increase), 1.1 (10% increase), etc as needed
#Has no effect if analysisPreset != 2
costScaleFactor = 1.05;
#Set the energy target to attempt to meet exactly
energyTarget = 2600.0

#Default input file names
fnPersonReq = "PersonReq.csv"
fnFoods = "Foods.csv"
#If analysisPreset is 5 then use the requirements with a different Potassium requirement.
if analysisPreset == 5
    fnPersonReq = "PersonReq_K3400mg.csv"
end

##############################
#Set the output directory name
##############################
if analysisPreset != 0
    analysisName = "analysis" * string(analysisPreset)
end

#Set up directories
analysisDir = joinpath(pwd(), analysisName)
resultsDir = analysisDir #Could make results directory different #joinpath( analysisDir, "results" )
mkpath(resultsDir)
#Results prefix can be adjusted if want to store results of different runs for the same "analysis" but otherwise can keep the same
resultsPrefix = "results"

#Read in nutrition requirements
#The column labels for the requirements must match exactly the column labels for the associated content in each food
#The model will only include nutrients which are included in the requirements file.
Reqs = CSV.read(joinpath(pwd(), fnPersonReq); copycols = true)
#Energy is set as an equality constraint using the energyTarget above so should NOT be included in the dataframe
for i in names(Reqs)
    if i == Symbol("Energy,kcal")
        select!(Reqs, Not(Symbol("Energy,kcal")))
        println("WARNING: Dropping energy requirements column from requirements dataframe as it is set via energyTarget in the code")
    end
end


#Read in nutritional data
#The foods database can have more columns than the requirements database
#Nutrient content without an associated requirement will be dropped
Foods = CSV.read(joinpath(pwd(), fnFoods); copycols = true)


#LP analysis 2 Increase in the cost for animal-based foods by 5, 10, 15 and 20 % #
if analysisPreset == 2
#change in cost for dairy foodgroup 10000
    for r in eachrow(Foods)
        if r.FoodGroup == 10000
            r.cost *= costScaleFactor
        end
    end
#change in cost for animal protein foodgroup 20000
    for r in eachrow(Foods)
        if r.FoodGroup == 20000
            r.cost *= costScaleFactor
        end
    end

#change in cost for animal-based salad dressings and mayonnaise foodGroup 71000
    for r in eachrow(Foods)
        if r.FoodGroup == 71000
            r.cost *= costScaleFactor
        end
    end

#change in cost for rolls and buns categorynumber 552
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 552
            r.cost *= costScaleFactor
        end
    end
end




# LP analysis 3 with price increase in animal products
if analysisPreset == 3
#change in cost for milk categoryGroupNumber 10
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 10
            r.cost *= 8
        end
    end



#change in cost for eggs categorygroupnumber 222
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 222
            r.cost *= 11.5
        end
    end


#change in cost for fish categorygroupnumber 291
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 291
            r.cost *= 6.5
        end
    end

#change in cost for animal-based salad dressings and mayonnaise foodGroup 71000
    for r in eachrow(Foods)
        if r[:FoodGroup] == 71000
            r.cost *= 5
        end
    end


#change in cost for rolls and buns categorynumber 552
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 552
            r.cost *= 4.5
        end
    end



# change in cost for beef categorygroupnumber 24
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 24
            r.cost *= 5.5
        end
    end


# change in cost for chicken categorygroupnumber 221
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 221
            r.cost *= 5
        end
    end


# change in cost for sausages categorygroupnumber 282
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 282
            r.cost *= 3
        end
    end



# change in cost for turkey categorygroupnumber 223
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 223
            r.cost *= 3
        end
    end


# change in cost for cheese categorygroupnumber 12
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 12
            r.cost *= 3
        end
    end


# change in cost for egg noodles categorygroupnumber 592
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 592
            r.cost *= 2
        end
    end


# change in cost for pork categorygroupnumber 25
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 25
            r.cost *= 2.5
        end
    end



# change in cost for cold cuts and cured meats categorygroupnumber 281
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 281
            r.cost *= 2
        end
    end



# change in cost for ice creams categorygroupnumber 83
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 83
            r.cost *= 2
        end
    end


# change in cost for yogurt categorygroupnumber 13
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 13
            r.cost *= 2.5
        end
    end

# change in cost for mashed potatoes categorygroupnumber 482 that included milk and butter
    for r in eachrow(Foods)
        if r.categoryGroupNumber == 482
            r.cost *= 2
        end
    end

end

#Store some information from the dataset for possible use later
foodNames = Foods[!, :Long_des]
foodShort = Foods[!, :Shortdes]
foodCodes = Foods[!, :Code]
foodGroup = Foods[!, :FoodGroup]
foodCost = Foods[!, :cost]
foodAmt = Foods[!, :reference_amount]
foodCatNum = Foods[!, :categoryGroupNumber]
foodEnergy = Foods[!, Symbol("Energy,kcal")]



#print how many foods are loaded
nFoods = size(Foods, 1)
println("$nFoods foods in the basket")


#Get full list of food names
println(names(Reqs))
println(names(Foods))


#If a column is not listed in the requirements file then drop it from the foods and from the analysis.
#Use the same symbol list "toInclude" in both Foods and Reqs to ensure consistency of data when building constraints later
#LP analysis 1-3#
#LP analysis 5 - change potassium from 4700 to 3400 mg#
toInclude = names(Reqs)


#LP analysis 4 - Requirements for all vitamins taken out. This could also be done just by providing a different PersonReqs file with those columns deleted
if analysisPreset == 4
    toInclude = [
        :Protein,
        Symbol("Total lipid (fat)"),
        Symbol("Carbohydrate, by difference"),
        Symbol("Fiber, total dietary"),
        Symbol("Calcium, Ca"),
        Symbol("Iron, Fe"),
        Symbol("Magnesium, Mg"),
        Symbol("Phosphorus, P"),
        Symbol("Potassium, K"),
        Symbol("Sodium, Na"),
        Symbol("Zinc, Zn"),
        Symbol("Copper, Cu"),
        Symbol("Manganese, Mn"),
        Symbol("Selenium, Se"),
        :Tryptophan,
        :Threonine,
        :Lysine,
        :Methionine,
        :Cystine,
        :Phenylalanine,
        :Tyrosine,
        :Valine,
        :Histidine,
        :Cholesterol,
        Symbol("18:2 n-6 c,c"),
        Symbol("18:3 n-3 c,c,c (ALA)"),
    ]
else
    toInclude = names(Reqs)
end

#Drop unneeded columns in foods database
Foods = Foods[:, toInclude]
Reqs = Reqs[:, toInclude]

#How many categories of nutrients are there?
nCats = size(toInclude, 1)
println("$nCats nutritional requirement categories")


#Get subset of nutrition values from Foods dataframe into an array
nutritionValues = Matrix(Foods);


#Prep constraints and nutritional data for model
nutrition = Matrix(Reqs)
minNutrition = nutrition[1, :] #minimum nutrition requirements is first row
maxNutrition = nutrition[2, :] #max nutrition requirements is second row


#Print to make sure it looks OK
println(minNutrition)
println(maxNutrition)

# Build model
println("Building model...")
m = Model(with_optimizer(GLPK.Optimizer))
#Create variable of foods to buy with minimum of zero and max of our serving size constraint
maxFoodAmt = 3 * foodAmt
@variable(m, 0 <= toBuy[i = 1:nFoods] <= maxFoodAmt[i])

#Set up objective function to minimise cost computed as
#sum of (quantity of food to buy * cost of that food) for all foods in basket
@objective(m, Min, dot(toBuy, foodCost))
println("Done")

#Set up nutrition constraints
println("Nutrition constraint")
@constraint(
    m,
    minNutCons[j = 1:nCats],
    sum(nutritionValues[i, j] * toBuy[i] for i = 1:nFoods) >= minNutrition[j],
)
@constraint(
    m,
    maxNutCons[j = 1:nCats],
    sum(nutritionValues[i, j] * toBuy[i] for i = 1:nFoods) <= maxNutrition[j],
)
println("Done")
println("Set up energy constraint")
@constraint(
    m,
    energyCons,
    sum(foodEnergy[i] * toBuy[i] for i = 1:nFoods) == energyTarget,
)
println("Done")


#change in serving size groups for food subgroup 552 rolls and buns and food subgroup 551 yeast breads
println("Food category group constraint...")
@expression(
    m,
    foodCatExp1[i = 1:nFoods; foodCatNum[i] == 552 || foodCatNum[i] == 551],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons1, sum(foodCatExp1) <= 2)
println("Done")


#change in serving size constraint for food subgroup 56 tortillas
println("Food category group constraint...")
@expression(
    m,
    foodCatExp2[i = 1:nFoods; foodCatNum[i] == 56],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons2, sum(foodCatExp2) <= 2)
println("Done")

#change in serving size constraint for food subgroup 20 legumes
println("Food category group constraint...")
@expression(
    m,
    foodCatExp3[i = 1:nFoods; foodCatNum[i] == 20],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons3, sum(foodCatExp3) <= 3)
println("Done")

#change in serving size constraint for food subgroup 58 rice
println("Food category group constraint...")
@expression(
    m,
    foodCatExp4[i = 1:nFoods; foodCatNum[i] == 58],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons4, sum(foodCatExp4) <= 2)
println("Done")

#change in serving size constraint for food subgroup 62 vegetable oils
println("Food category group constraint...")
@expression(
    m,
    foodCatExp5[i = 1:nFoods; foodCatNum[i] == 62],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons5, sum(foodCatExp5) <= 3)
println("Done")

#change in serving size constraint for food subgroup 63 margarine
println("Food category group constraint...")
@expression(
    m,
    foodCatExp6[i = 1:nFoods; foodCatNum[i] == 63],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons6, sum(foodCatExp6) <= 1)
println("Done")


#change in serving size constraint for foodsubgroup 81 sugars
println("Food category group constraint...")
@expression(
    m,
    foodCatExp7[i = 1:nFoods; foodCatNum[i] == 81],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons7, sum(foodCatExp7) <= 3)
println("Done")

#change in serving size constraint for food subgroup 481 482 potatoes
println("Food category group constraint...")
@expression(
    m,
    foodCatExp8[i = 1:nFoods; foodCatNum[i] == 481 || foodCatNum[i] == 482],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons8, sum(foodCatExp8) <= 3)
#println("Done")

#change in serving size constraint for food subgroup 10 milk
println("Food category group constraint...")
@expression(
    m,
    foodCatExp9[i = 1:nFoods; foodCatNum[i] == 10],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons9, sum(foodCatExp9) <= 3)
println("Done")



#change in serving size constraint for food subgroup 2111 peanut butter
println("Food category group constraint...")
@expression(
    m,
    foodCatExp10[i = 1:nFoods; foodCatNum[i] == 2111],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons10, sum(foodCatExp10) <= 1)
println("Done")


#change in serving size constraint for food subgroup 100 milk substitutes
println("Food category group constraint...")
@expression(
    m,
    foodCatExp11[i = 1:nFoods; foodCatNum[i] == 100],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons11, sum(foodCatExp11) <= 3)
println("Done")


#change in serving size constraint for food subgroup 47 tomatoes
println("Food category group constraint...")
@expression(
    m,
    foodCatExp12[i = 1:nFoods; foodCatNum[i] == 47],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons12, sum(foodCatExp12) <= 3)
println("Done")

#change in serving size constraint for foodsubgroup 75 salad dressings including mayonnaise
println("Food category group constraint...")
@expression(
    m,
    foodCatExp13[i = 1:nFoods; foodCatNum[i] == 75],
    toBuy[i] / foodAmt[i],
)
@constraint(m, foodCatCons13, sum(foodCatExp13) <= 1)
println("Done")



#Solve system
println("Solving...")
optimize!(m)
println("Solver finished with status ", termination_status(m))
println("Primal status ", primal_status(m))
println("Dual status ", dual_status(m))
println("Objective value (Cost of optimal diet in \$): ", objective_value(m))

################################
#Set up some summary dataframes
################################
#Dataframe for storing serving size summary for food groups
theFoodGroups = unique(foodGroup) #This just grabs the actual unique names..
foodGroupServings = DataFrame()
#Initialize quantities to zero
for i = 1:length(theFoodGroups)
    sym = Symbol(theFoodGroups[i])
    foodGroupServings[!, sym] = [0]
end

#Set up dataframe for holding foods contained in optimum diet
optimDiet = DataFrame(
    Code = Int64[],
    Name = String[],
    foodGroup = Int64[],
    Quantity = Float64[],
    Servings = Float64[],
    Cost = Float64[],
) #Always want Name in dataframe.
#First do energy
sym = Symbol("Energy,kcal")
optimDiet[!, sym] = Float64[]
sym = Symbol("Energy,kcal (% min. daily req.)")
optimDiet[!, sym] = Float64[]
for j = 1:nCats
    sym = Symbol(names(Foods)[j])
    optimDiet[!, sym] = Float64[]
    sym = Symbol(names(Foods)[j], " (% min. daily req.)")
    optimDiet[!, sym] = Float64[]
end

println("Model has duals: ", has_duals(m))



total = fill(0.0, nCats)


totalEnergy = 0.0
println("Gathering results and writing to file...")

#Loop over all foods in basket
for i = 1:length(foodNames)
#But only interested if we actually purchase it
#Use value because toBuy is a variable in the model m so not directly available.
    if (value(toBuy[i]) > 1.0e-12) # Effectively 0, sometimes seeing some quantities in the 1e-16 range

#Sum up totals of each nutrient consumed for optimum diet summary
        for j = 1:nCats
            total[j] += nutritionValues[i, j] * value(toBuy[i])
        end
        global totalEnergy += foodEnergy[i] * value(toBuy[i])


#Save information about thisFood in a temporary dataframe.
#Columns need to be the same as what we set up above for optimDiet dataframe
        thisFood = DataFrame(
            Code = foodCodes[i],
            Name = foodNames[i],
            foodGroup = foodGroup[i],
            Quantity = value(toBuy[i]),
            Servings = value(toBuy[i]) / foodAmt[i],
            Cost = value(toBuy[i]) * foodCost[i],
        )

#

#First do energy
        sym = Symbol("Energy,kcal")
        thisFood[!, sym] = [value(toBuy[i]) * foodEnergy[i]]
        sym = Symbol("Energy,kcal (% min. daily req.)")
        thisFood[!, sym] = [value(toBuy[i]) * foodEnergy[i] / energyTarget *
                            100]
        idx = 0
#Now do the rest
        for j = 1:nCats
            idx = idx + 1
            sym = Symbol(names(Foods)[j])
            thisFood[!, sym] = [value(toBuy[i]) * nutritionValues[i, idx]]
            sym = Symbol(names(Foods)[j], " (% min. daily req.)")
            thisFood[!, sym] = [value(toBuy[i]) * nutritionValues[i, idx] /
                                minNutrition[idx] * 100]
        end

#Append thisFood information to the overall optimDiet dataframe
        append!(optimDiet, thisFood)

#For food group serving summaries
#First get column name (food group that this food belongs to)
        sym = Symbol(foodGroup[i])
#Add this serving to the amount consumed where
#num. of servings=(quantity to buy)/(serving size quantity)
        foodGroupServings[!, sym] += [value(toBuy[i] / foodAmt[i])]

    end
end


#Save data to summary
#sensitivities of the minimum nutrition constraints
# See https://www.juliaopt.org/JuMP.jl/stable/solutions/#JuMP.lp_rhs_perturbation_range
#First do energy
#Just set min and max to the same so we can include it on the table with other nutrients.

optimDietSummary = DataFrame(
    nutritionName = String("Energy,kcal"),
    sumConsumed = totalEnergy,
    sumConsumedPctOfMinReq = totalEnergy / energyTarget,
    minConstraint = energyTarget,
    minConstraintShadowPrice = shadow_price.(energyCons),
    # minConstraintValidDelta = lp_rhs_perturbation_range.(energyCons),
    maxConstraint = energyTarget,
    maxConstraintShadowPrice = shadow_price.(energyCons),
    #maxConstraintValidDelta = lp_rhs_perturbation_range.(energyCons),
)

nutritionDF = DataFrame(
    nutritionName = String.(names(Foods)),
    sumConsumed = total,
    sumConsumedPctOfMinReq = total ./ minNutrition,
    minConstraint = minNutrition,
    minConstraintShadowPrice = shadow_price.(minNutCons),
    # minConstraintValidDelta = lp_rhs_perturbation_range.(minNutCons),
    maxConstraint = maxNutrition,
    maxConstraintShadowPrice = shadow_price.(maxNutCons),
    # maxConstraintValidDelta = lp_rhs_perturbation_range.(maxNutCons),
)

append!(optimDietSummary, nutritionDF)

CSV.write(
    quotestrings = true,
    joinpath(resultsDir, resultsPrefix * "_optimDietSummary.csv"),
    optimDietSummary,
)

#Rearrange food group serving size summaries to a cleaner two column dataframe
tmp = DataFrame(foodGroup = Vector(names(foodGroupServings)))
tmp2 = DataFrame(transpose(Matrix(foodGroupServings[:, :])))
#For some reason can't seem to set the name directly in tmp2 call above
rename!(tmp2, :x1 => :servingSums)

#Summary of serving size consumed by food group
# CSV.write(
#     joinpath(resultsDir, resultsPrefix * "_optimDietServingSummary.csv"),
#     hcat(tmp, tmp2),
# )

#Output the actual diet
#Just output foods, their quantity, cost, and energy
outFile = joinpath(resultsDir, resultsPrefix * "_optimDiet.csv")
optimDietOut = optimDiet[
    :,
    [
     :Code,
     :Name,
     :foodGroup,
     :Quantity,
     :Servings,
     :Cost,
     Symbol("Energy,kcal"),
    ],
]
CSV.write(
    outFile,
    optimDietOut,
    quotestrings = true,
    header = string.(names(optimDietOut)),
)

println("Done")



#Reduced costs for the food items
#Sensitivities of the food serving size constraints
foodAmtConsDF = DataFrame(
    Code = foodCodes,
    Food = foodNames,
    foodAmtShadowPrice = shadow_price.(UpperBoundRef.(toBuy)),
    currentConstraint = maxFoodAmt,
    # validConstraintRange = lp_rhs_perturbation_range.(UpperBoundRef.(toBuy)),
)
CSV.write(
    quotestrings = true,
    joinpath(resultsDir, resultsPrefix * "_foodAmtShadowPrice.csv"),
    foodAmtConsDF[foodAmtConsDF.foodAmtShadowPrice.!=0, :],
);
